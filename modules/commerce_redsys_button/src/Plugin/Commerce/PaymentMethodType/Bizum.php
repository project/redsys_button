<?php

namespace Drupal\commerce_redsys_button\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\FailedPaymentDetailsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the Bizum payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "bizum",
 *   label = @Translation("Bizum"),
 * )
 */
class Bizum extends PaymentMethodTypeBase implements FailedPaymentDetailsInterface {

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreateLabel() {
    return $this->pluginDefinition['create_label'] ?? $this->t('Add Bizum payment method');
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    // Define any additional fields for the Bizum payment method, if required.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->t('Bizum Payment');
  }

  /**
   * {@inheritdoc}
   */
  public function buildFailedPaymentDetails() {
    return [
      'reason' => $this->t('The Bizum payment could not be processed.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function failedPaymentDetails(PaymentMethodInterface $payment_method): array {
    return [
      'reason' => $this->t('The Bizum payment could not be processed.'),
      'transaction_id' => $payment_method->getRemoteId(),
    ];
  }

}
